<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Interfaces\RouteCollectorInterface;
use Twig\Environment;

class TrailerController
{
    /**
     * Initialize properties.
     *
     * @param RouteCollectorInterface $routeCollector
     * @param Environment $twig
     * @param EntityManagerInterface $em
     */
    public function __construct(
        private RouteCollectorInterface $routeCollector,
        private Environment $twig,
        private EntityManagerInterface $em
    ) {
    }

    /**
     * Find and show single trailer.
     *
     * @throws \Twig\Error\SyntaxError
     * @throws HttpNotFoundException
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\LoaderError
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $trailerId = (int) $request->getAttribute('id');

        $data = $this->twig->render('trailer/index.html.twig', [
            'trailer' => $this->findOrFail($request, $trailerId),
        ]);

        $response->getBody()->write($data);

        return $response;
    }

    /**
     * Find model or throw HttpNotFoundException.
     *
     * @param ServerRequestInterface $request
     * @param int $trailerId
     * @return Movie
     * @throws HttpNotFoundException
     */
    protected function findOrFail(ServerRequestInterface $request, int $trailerId): Movie
    {
        return $this->em
                ->getRepository(Movie::class)
                ->find($trailerId) ?? throw new HttpNotFoundException($request);
    }
}
